# TennisGameKata

This is a simple Tennis Game built in Java language

This project is built in Java version 1.8 using NetBeans IDE.

It is a Tennis Game developed as per the description of the game at:
https://gist.github.com/MatteoPierro/22e09a2b5d9e41fdd8c226d318fc0984.

Refer to index.html in Javadoc folder for description of each class.
Unzip the javadoc zip file and click on index.html page.
Click on each class on the side panel of the html file.


Running the application using a IDE like NetBeans:

The Zip file: TennisGame.zip can be extracted and imported into any IDE like Eclipse or NetBeans.
TennisGameLauncher.java is the main class containing the main method. This class is the starting point used to launch the game.

Junit testing:
GameTest.java file in the Test folder is used to Junit test the code.
It has 5 methods for different scenarios to test the overall functionality.

Steps to run this application using command line:

To run this application from the command line without Ant, run:
java -jar "Path to the file\TennisGame.jar"

where Path to the file is for the full location where the file TennisGame.jar is placed.





