/*
 * This test class is used to test the outcome of the game in different 
 * point situations. It tests the methods initializeGame,readScore,
 * incrementPlayer1Score,incrementPlayer2Score,announceWinner methods of
 * the Game class.
 */
package Test;


import com.capgemini.main.TennisGame.Game;
import com.capgemini.main.TennisGame.Player;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author XXXXXX
 */
public class GameTest {

    static Game instance = new Game();
    static Player player1;
    static Player player2;

    // Initial Setup . This will run before each test method call.
    @Before
    public void setUp() {

        player1 = new Player("Roger", "Federer", 1);
        player2 = new Player("Novak", "Djokovic", 2);
        instance.initializeGame(player1, player2);  // Initializing the players scores to 0.0
        instance.readScore();  //Reading players initial scores.
    }
  //  Testing player 1 wins the game 
    @Test
    public void testingPlayer1Wins() {

        instance.incrementPlayer1Score();   // 15:0
        instance.incrementPlayer2Score();   // 15:15
        instance.incrementPlayer1Score();   // 30:15
        instance.incrementPlayer1Score();   //40:15
        instance.incrementPlayer1Score();   // W:15
        instance.announceWinner();          // Roger Wins

    }
 //  Testing player 2 wins the game 
    @Test
    public void testingPlayer2Wins() {

        instance.incrementPlayer1Score();    // 15:0
        instance.incrementPlayer2Score();  // 15:15
        instance.incrementPlayer1Score();   // 30:15
        instance.incrementPlayer2Score();   // 30:30
        instance.incrementPlayer2Score();  //  30:40
        instance.incrementPlayer2Score();   // 30:W
        instance.announceWinner();              //Novac Wins

    }
    //  Testing player 1 wins the game 
    @Test
    public void testingPlayer1WinsAgain() {

        instance.incrementPlayer1Score();  // 15:0
        instance.incrementPlayer2Score();  //  15:15
        instance.incrementPlayer1Score();  //  30:15
        instance.incrementPlayer1Score();  //  40:15
        instance.incrementPlayer1Score();  //  W:15
        instance.announceWinner();         // Roger Wins

    }
    //  Testing player 2 wins the game 
    @Test
    public void testingPlayer2WinsAgain() {

        instance.incrementPlayer1Score();  //15:0
        instance.incrementPlayer2Score();  //15:15
        instance.incrementPlayer2Score();  //15:30
        instance.incrementPlayer1Score();  //30:30
        instance.incrementPlayer1Score();  //40:30
        instance.incrementPlayer2Score();  //40:40
        instance.incrementPlayer2Score();  //40:A
        instance.incrementPlayer2Score();  //40:W 
        instance.announceWinner();         // Novac Wins

    }
    //  Testing player 1 wins the game 
    @Test
    public void testingPlayer1WinsOncemore() {

        instance.incrementPlayer1Score();  //  15:0
        instance.incrementPlayer2Score();  //  15:15
        instance.incrementPlayer2Score();  //  15:30
        instance.incrementPlayer2Score();  //  15:40
        instance.incrementPlayer1Score();  //  30:40
        instance.incrementPlayer1Score();   // 40:40
        instance.incrementPlayer1Score();  //  A:40
        instance.incrementPlayer1Score();   // W:40
        instance.announceWinner();          //  Roger Wins

    }

}
