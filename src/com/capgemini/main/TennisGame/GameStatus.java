/*
 * Enum to store the game status.
 */
package com.capgemini.main.TennisGame;

/**
 *
 * @author ****
 */
public enum GameStatus {

    Scheduled,
    InProgress,
    Completed,
    Canceled

}
