/*
 * Enum to store the Score of the players.
 */
package com.capgemini.main.TennisGame;

/**
 *
 * @author ****
 */
public enum Score {

    Love("0"),
    Fifteen("15"),
    Thirty("30"),
    Fourty("40"),
    Advantage("A"),
    Win("W");

    private String value;

    private Score(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
;

}
