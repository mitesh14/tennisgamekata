/*
 * This class is used to store Player details.
 * 
 * 
 */
package com.capgemini.main.TennisGame;

/**
 *
 * @author *****
 */
public class Player {

    public String firstName;
    public String lastName;
    public int rank;

    public Player(String firstName, String lastName, int rank) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.rank = rank;
    }
}
