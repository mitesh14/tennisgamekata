/*
 * Launcher (Main) class to launch the game.
 * It randomly assigns points to player using the class Random.
 */
package com.capgemini.main.TennisGame;

import java.util.Random;

/**
 *
 * @author *****
 */
public class TennisGameLauncher {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Player player1 = new Player("Roger", "Federer", 1);
        Player player2 = new Player("Novak", "Djokovic", 2);
        Game game = new Game();
        game.initializeGame(player1, player2);
        Random rand = new Random();
        while (game.status != GameStatus.Completed) {
            game.readScore();
            if (rand.nextInt(2) % 2 == 0) {
                System.out.println(player1.firstName + " wins a point");
                game.incrementPlayer1Score();
            } else {
                System.out.println(player2.firstName + " wins a point");
                game.incrementPlayer2Score();
            }
        }
        game.announceWinner();
    }

}
