/*
 * This is main class which provides the funtionality for the Tennis Game.
 * It increments the score of the players and decides the winner
 */
package com.capgemini.main.TennisGame;

/**
 *
 * @author ****
 */
public class Game {

    Player player1;
    Player player2;
    Score player1Score;
    Score player2Score;
    GameStatus status;
    Player winner;

    public void initializeGame(Player player1, Player player2) {

        System.out.println("Welcome to Kata Tennis. \nToday's game against " + player1.firstName + " " + player1.lastName + " and " + player2.firstName + " " + player2.lastName);
        this.player1 = player1;
        this.player2 = player2;
        this.player1Score = Score.Love;
        this.player2Score = Score.Love;
        status = GameStatus.Scheduled;
    }

    ;
    
    public void readScore() {

        if (this.player1Score == Score.Win || this.player2Score == Score.Win) {
            announceWinner();
            return;
        }

        System.out.println("\n" + player1.firstName + ": " + player1Score.getValue() + "\n" + player2.firstName + ": " + player2Score.getValue() + "\n");
        if (this.player1Score == this.player2Score && this.player1Score == Score.Fourty) {
            System.out.print("The game is at Deuce\n\n");
        }
    }

    public Score getScore(Player player) {
        if (player == player1) {
            return player1Score;
        } else {
            return player2Score;
        }
    }

    public void incrementPlayer1Score() {
        switch (this.player1Score) {
            case Love:
            case Fifteen:
            case Thirty:
                this.player1Score = Score.values()[this.player1Score.ordinal() + 1];
                break;
            case Fourty:
                if (this.player2Score == Score.Fourty) {
                    this.player1Score = Score.Advantage;
                } else if (this.player2Score == Score.Advantage) {
                    this.player1Score = Score.Fourty;
                    this.player2Score = Score.Fourty;
                } else {
                    this.player1Score = Score.Win;
                    this.status = GameStatus.Completed;
                    this.setWinner(this.player1);
                }
                break;
            case Advantage:
                this.player1Score = Score.Win;
                this.status = GameStatus.Completed;
                this.setWinner(this.player1);
                break;
        }
    }

    public void incrementPlayer2Score() {
        switch (this.player2Score) {
            case Love:
            case Fifteen:
            case Thirty:
                this.player2Score = Score.values()[this.player2Score.ordinal() + 1];
                break;
            case Fourty:
                if (this.player1Score == Score.Fourty) {
                    this.player2Score = Score.Advantage;
                } else if (this.player1Score == Score.Advantage) {
                    this.player2Score = Score.Fourty;
                    this.player1Score = Score.Fourty;
                } else {
                    this.player2Score = Score.Win;
                    this.status = GameStatus.Completed;
                    this.setWinner(this.player2);
                }
                break;
            case Advantage:
                this.player2Score = Score.Win;
                this.status = GameStatus.Completed;
                this.setWinner(this.player2);
                break;
        }
    }

    public void setWinner(Player player) {
        this.winner = player;
    }

    public void announceWinner() {
        if (this.winner == null) {
            return; // or throw exception
        }

        System.out.println("\n" + player1.firstName + ": " + player1Score.getValue() + "\n" + player2.firstName + ": " + player2Score.getValue());
        System.out.println("\n" + winner.firstName + " " + winner.lastName + " has won the game" + "\n");
    }
}
